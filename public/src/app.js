/**
 * Created by sebast on 10/12/15.
 */
angular.module('PrintApp', ['ngFileUpload'])

    .controller('FileController', ['$scope', 'Upload', function($scope, Upload) {
        //var $scope = this;
        $scope.printFiles = [];
        $scope.printedFiles = [];
        $scope.printSuccess = false;
        $scope.printError = false;
        $scope.isUploading = false;
        $scope.progress = 0;

        $scope.submit = function() {
            console.log('submit');
            $scope.upload($scope.printFiles);
            $scope.isUploading = true;
        };

        $scope.delete = function(file) {
            $scope.printFiles.splice(file, 1);
        };

        // upload on file select or drop
        $scope.upload = function (file) {
            console.log(file);
            Upload.upload({
                url: '/upload/',
                arrayKey: '',
                data: {printFiles: [file]}
            }).then(function (resp) {
                // Success
                console.log('Success ' + resp.config.data.printFiles.name + 'uploaded. Response: ' + resp.data);
                $scope.printSuccess = true;
                $scope.printError = false;
                $scope.isUploading = false;
                //$scope.progress = 0;

                // Remove files
                $scope.printedFiles = $scope.printFiles;
                $scope.printFiles = [];
                console.log(resp);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
                $scope.printSuccess = false;
                $scope.printError = true;
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.progress = progressPercentage;
                //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.printFiles.name);
            });
        };
    }])

    .controller('HistoryController', [ '$http', '$interval', function($http, $interval) {
        var vm = this;
        this.orderName = 'date';
        this.orderReverse = true;
        vm.log = [];

        $interval(function() {
            $http.get('/api/printLog').then(function(data) {
                vm.log = data.data;
            });
        }, 1000);
    }])

    .filter('bytes', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
            if (typeof precision === 'undefined') precision = 1;
            var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes / Math.pow(1000, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    });

;