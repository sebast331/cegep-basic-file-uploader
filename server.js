/**
 * Created by sebast on 10/12/15.
 */

var express     = require('express'),
    multer      = require('multer'),
    lowdb       = require('lowdb'),
    db          = lowdb('dbPrintLog.json'),
    upload      = multer({
                    storage: multer.diskStorage({
                        destination: function (req, file, cb) {
                            cb(null, './uploads/');
                        },
                        filename: function (req, file, cb) {
                            cb(null, Date.now() + '-' + file.originalname);
                        }
                    })
        }),
    app         = express();



var printLog = [];

app
    .set('port', 3000)
    .use(express.static('public'))
    .post('/upload', upload.array('printFiles'), function(req, res) {
        req.files.forEach(function(k, v) {
            var newLog = {
                name: k.originalname,
                ip: req.connection.remoteAddress.split(':').pop(),
                date: new Date(),
                size: k.size,
            };
            printLog.push(newLog);
            db('printLog').push(newLog);
        });
        res.redirect('/');
    })
    .get('/api/printLog', function(req, res) {
        res.send(db('printLog').take(db('printLog').size()));
    })
    .get('/', function(req, res) {
        res.sendFile('public/main.html', {root: __dirname });
    })
    .listen(app.get('port'), function() {
        console.log('Server started on port ' + app.get('port') + ' Press CTRL+C to terminate');
    });