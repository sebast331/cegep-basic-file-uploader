# README #

## Installation ##
1. Cloner à l'aide de la commande git clone
2. Installer les prérequis
3. Exécuter le serveur

```bash
$ git clone https://bitbucket.org/sebast331/cegep-basic-file-uploader
$ npm install
$ cd cegep-basic-file-uploader && node server.js
```